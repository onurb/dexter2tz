# USDtz

USDtz is a stable coin token that represents USD on the Tezos blockchain in a
tradeable form. It adheres to the [FA1.2 specification](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md) 
and it uses [output code](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/ManagedLedger.tz) 
from the [Morley library](https://gitlab.com/morley-framework/morley/).

The two contracts of interest are [USDtz](https://better-call.dev/mainnet/KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9) and
[Dexter USDtz/XTZ ](https://better-call.dev/mainnet/KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD/operations).

For more information, visit the [USDtz website](https://usdtz.com/).

# Token integration checklist

Each token contract must be evaluated individually before originating a new Dexter 
exchange contract for it, you need to consider the risks associated with it and 
provide this knowledge publicly. FA1.2 is a light  specification on the required 
behavior of a token contract. It leaves a lot of room for behavior that would be 
incompatible with creating a safe Dexter exchange. 

For each FA1.2 contract integrated into a Dexter exchange contract, you 
should publish and link this checklist filled out with the details for it. 
Add detailed comments for each question below.

Anything not checked is considered a vulnerability. Having a vulnerability
does not mean that the Dexter exchange contract should not be created, rather it
is to warn users of possible weaknesses of the FA1.2 contract. It is up to them to
decide whether or not to support the contract.

This document is based off of Trail of Bits' 
[Ethereum token integration checklist](https://github.com/crytic/building-secure-contracts/blob/c6ff933c0ce56e16edaaaf2160f01fd2620d52e1/development-guidelines/token_integration.md).

## General considerations

- [ ] **The FA1.2 contract has a security review.** Avoid interacting with contracts 
  that have not had a security review.
  
To our knowledge there has not been a formal security review of the [code](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/ManagedLedger.tz) 
that was used to originate the USDtz contract.

- [x] **The FA1.2 contract is documented.** It should explain how the contract works
  and how to interact with it.

This [documentation](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/ManagedLedger.md)
corresponds to the code that was used to originate the USDtz contract. There is a new set of 
[documentation](https://gitlab.com/morley-framework/morley-ledgers/blob/autodoc/master/autodoc/ManagedLedger.md) 
for the latest managed ledger code, but it may differ from the code that was used to originate
USDtz.

- [x] **There are clear steps how to originate the contract on the Tezos test net.**

Managed ledger is a simple contract. If you know how to originate a smart 
contract with the tezos client, it should be clear how to originate a managed ledger
contract that matches USDtz using the following 
[documentation](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/ManagedLedger.md#deployment).

- [ ] **Contact the developers of the FA1.2 contract.** There maybe known issues 
  that they have not documented. There may be peculiar behaviors that you should
  know about.
  
Contact is pending.
  
- [ ] **They have a security mailing list for critical announcements.** Their team 
  should advise users (like you!) when critical issues are found or when 
  upgrades occur.
  
There is not a mailling list that we know of.

## FA1.2 conformity


- [x] **The FA1.2 contract conforms to all of the specifications of 
  [tzip-7](https://gitlab.com/tzip/tzip/-/blob/d53952592717ef56ce45cc80d7231bec5c12592b/proposals/tzip-7/tzip-7.md)
  (The FA1.2 specification).**

## Contract composition

- [x] **Do not interface Dexter with FA1.2 
  contracts that perform internal operations when getBalance is called.**
  Dexter uses the getBalance entrypoint of FA1.2 internally. Due to the 
  message passing architecture of Tezos (https://forum.tezosagora.org/t/smart-contract-vulnerabilities-due-to-tezos-message-passing-architecture/2045)
  which allows for reordering of internal operations, Dexter can only safely call 
  getBalance of FA1.2 tokens that forward this value  without
  performing calls to other contracts (For example, the FA1.2 contract is 
  composed of two contracts and must query the second contract to calculate the balance). 
  Such a contract would allow attackers to nsert operations and potentially change the 
  expected forwarded value of getBalance. 

USDtz is composed of a single contract.

- [x] **Do not interface Dexter with FA1.2 contracts that perform internal 
  operations when transfer is called**. Moreover, transfer should immediately
  update the balances when called. Same reasoning as above.

USDtz is composed of a single contract.

- [x] **If the contract was not written in Michelson, the 
  author should publish the code that was used to generate the Michelson 
  contract that was originated on Tezos.**

From our understanding, this [output Michelson code](https://gitlab.com/tzip/tzip/-/blob/5d3e60c437ad33e3b5dfe3e8654188a42c68485a/proposals/tzip-7/ManagedLedger.tz)
was used to produce USDtz. However, it is unclear which version of morley was 
used to produce this code. We recommend that the authors publish this information
on [USDtz's public website](https://usdtz.com/).


- [ ] **The contract code is well commented.** It should be relatively 
  straightforward for experienced contract developers to understand
  the purpose of the code in the contract. The contract avoids unnecessary 
  complexity.

The managed ledger code we found [here](https://gitlab.com/morley-framework/morley-ledgers/-/blob/c04bcfc21db3dcd7922d35483a3fc3888435f244/code/morley-ledgers/src/Lorentz/Contracts/ManagedLedger/Impl.hs) 
is well commented, but we are not sure if this is the same version to used to produce USDtz or not.
Pending communication with the responsible parties.

## Owner privileges

- [x] **The token is not upgradeable.** Upgradeable contracts might change their 
  rules over time.

USDtz's code is not upgradeable 

- [ ] **The token is not pausable.** Malicious or compromised owners can trap 
  contracts relying on pausable tokens. Identify pauseable code by hand.

USDtz is pausable. Tokens can be minted and burned. The USDtz [Minteries](https://usdtz.com/minteries.html)
have the power to create new tokens.

- [x] **The owner cannot blacklist the contract.** Malicious or compromised 
  owners can trap contracts relying on tokens with a blacklist. Identify 
  blacklisting features by hand.

- [x] **The team behind the token is known and can be held responsible for 
  abuse.** Contracts with anonymous development teams, or that reside in 
  legal shelters should require a higher standard of review.

Responsibility according to the [USDtz website](https://usdtz.com/): 
"These efforts are coordinated under the aegis of the Tezos Stable Technologies, 
Ltd. (aka StableTech), which governed by The Tezos Stablecoin Foundation, a 
non-profit organization."

[Serokell](https://serokell.io/) is responsible for designing the Managed Ledger
contract.

## Token scarcity

- [x] **No user owns most of the supply.** If a few users own most of the tokens, 
  they can influence operations based on the token's repartition.

The addresses that own USDtz are visible here: 
[USDtz token owners](https://better-call.dev/mainnet/KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9/tokens).

- [x] **The total supply is sufficient.** Tokens with a low total supply can be 
  easily manipulated. The lower the balance of token or XTZ in a dexter exchange
  contract, the easier it is to manipulate the exchange rate. If there is a low
  total supply, then the maximum token pool will be low. In our experience, token
  pools of less than 100,000 in Dexter are easy to manipulate. Depending on the
  nature of the contract, a total supply of 100,000 is likely to low, if it is
  300,000 and the majority will be in Dexter, than that is good, otherwise consider
  how much is likely to end up in Dexter.

There is a sufficient number of tokens.

- [x] **The tokens are located in more than a few exchanges.** If all the tokens are 
  in one exchange, a compromise of the exchange can compromise the contract 
  relying on the token.

There are multiple providers of USDtz.

- [x] **The token does not allow flash minting.** Flash minting can lead to
  substantial swings in the balance and the total supply, which 
  neccessitate strict and comprehensive overflow checks in the operation of the 
  token.

Minting must go through confirmed providers.
