# Terminology

Terms used throughout the informal specifications of Dexter 2:

* **XTZ**: the official token on the Tezos blockchain. Also known as
  tez.  The smallest unit is mutez. The common unit is tez. 1 tez =
  1,000,000 mutez. For clarity, we will refer to this token as XTZ,
  and we will not refer to it as a token.
* **FA1.2**: tokens from a [FA1.2 contract][fa12].
* **FA2**: tokens from a [FA2 contract][fa2].
* **TOK**: tokens from a [FA1.2 contract][fa12]
  or a [FA2 contract][fa2]. In the rest of
  this document, token will never refer to XTZ.
* **token contract**: a contract that adheres to one of the following standards:
  [FA1.2][fa12]
  or [FA2][fa2].
* **LQT**: a token that is internal to Dexter 2, handled by the
  accompanying liquidity token contract (see [`dexter2.md`][dexter2] for an
  overview of Dexter 2 contracts and [`dexter2-lqt-fa12.md`][dexter2-lqt-fa12] for the informal
  specification of the liquidity token contract). It represents a share of the amount **XTZ** and
  **TOK** held by Dexter 2.
* **exchange**: a Dexter 2 instantiation, enabling decentralized
  exchange on Tezos between **XTZ** and a particular **token contract**.
* **CPMM contract**: the Dexter 2 CPMM contract of an **exchange**
* **xtzPool**: amount of **XTZ** held by the CPMM contract in mutez, either
  deposited by the liquidity providers against **LQT** or exchanged against **TOK** by
  traders. This value differs from that returned by the Michelson
  instruction [`BALANCE`][mref-BALANCE], in that it does not contain
  the amount of previously emitted, outgoing but not yet executed
  transactions from the CPMM. [^1]
* **tokenPool**: amount of **TOK** held by the CPMM contract, either
  deposited by the liquidity providers against **LQT** or exchanged
  against **XTZ** by traders. The exchange may actually hold more
  **TOK** than the **tokenPool** stored value for reasons explained
  below.
* **lqtTotal**: total amount of **LQT** held by the liquidity providers of the exchange.
  **LQT** is owned by the liquidity
  providers. It is not owned by the exchange itself.
* **allowance**: mechanism of a token contract whereby one **address**
  (the owner) gives permission to another **address** (the spender) to
  transfer the owner's token.
* **manager**: a privileged address that can set the delegate
  (referred to as the baker) of the CPMM contract, unless the baker
  has been frozen.
* **implicit account**: A Tezos address starting with `tz` that can
  hold **XTZ**, transfer it and call **contract** entrypoints. It has
  no associated smart contract.
* **address**: A unique string identifier of an **implicit account**
  or a **contract**.
* **contract**: A tezos address starting with `KT` that has an
  associated smart contract. It can only initiate a transfer or
  call to another **contract** when called in a chain of calls
  initiated by an **implicit account**.
* **balance**: Amount of XTZ held by a given **implicit account** or a
  **contract**.
* **view entrypoint**: a Tezos smart contract entrypoint of a specific
  form for querying a smart contract and retrieving the response
  through a callback. See [TZIP-4][tzip-4-views] for more information.

[^1]: The CPMM tracks this number because the operation [BALANCE][mref-BALANCE] is
    considered unreliable ([Balance
    Exploit](https://gitlab.com/camlcase-dev/contract-experiments/-/blob/737e6ca0c86970ffae860859028b203e7b685845/receiver-attack/README.md)).

[dexter2]: dexter2.md
[dexter2-lqt-fa12]: dexter2-lqt-fa12.md
[fa12]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md "TZIP-7: The FA1.2 standard"
[fa2]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md "TZIP-12: The FA2 standard"
[tzip-4-views]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-4/tzip-4.md#view-entrypoints "TZIP-4: Michelson Contract Interfaces and Conventions, section View entrypoints"
[mref-AMOUNT]: https://tezos.gitlab.io/michelson-reference/#instr-AMOUNT "The Michelson AMOUNT instruction"
[mref-BALANCE]: https://tezos.gitlab.io/michelson-reference/#instr-BALANCE "The Michelson BALANCE instruction"
