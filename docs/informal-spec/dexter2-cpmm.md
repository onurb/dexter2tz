# Dexter 2 Exchange: Informal specification of the CPMM contract

This document is an informal specification for the CPMM contract of
Dexter 2 (contract hash
`expruahNDMqQxdsrBoyUF7pC1z6qGSRofgtFtgL8MgTsAe74n8FSdc`). See
[`dexter2`][dexter2] for an overview of Dexter 2.  See
[`dexter2-terminology`][dexter2-terminology] for a review of the
terminology used throughout this specification.

## Errors

If an error condition is triggered (through the Michelson
[`FAILWITH`][mref-FAILWITH]
instruction), then a `nat` containing an error code is given.

If a division by zero occurs, the error string `"DIV by 0"` is given.

The meaning of error codes is the following:

 | Code | Name                                                                                   | Comment                    |
 |:-----|:---------------------------------------------------------------------------------------|:---------------------------|
 | 0    | `TOKEN_CONTRACT_MUST_HAVE_A_TRANSFER_ENTRYPOINT`                                       |                            |
 | 2    | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE`                                            |                            |
 | 3    | `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`                                      |                            |
 | 4    | `MAX_TOKENS_DEPOSITED_MUST_BE_GREATER_THAN_OR_EQUAL_TO_TOKENS_DEPOSITED`               |                            |
 | 5    | `LQT_MINTED_MUST_BE_GREATER_THAN_MIN_LQT_MINTED`                                       |                            |
 | 8    | `XTZ_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_BOUGHT`                           |                            |
 | 9    | `INVALID_TO_ADDRESS`                                                                   |                            |
 | 10   | `AMOUNT_MUST_BE_ZERO`                                                                  |                            |
 | 11   | `THE_AMOUNT_OF_XTZ_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_WITHDRAWN`       |                            |
 | 12   | `LQT_CONTRACT_MUST_HAVE_A_MINT_OR_BURN_ENTRYPOINT`                                     |                            |
 | 13   | `THE_AMOUNT_OF_TOKENS_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_WITHDRAWN` |                            |
 | 14   | `CANNOT_BURN_MORE_THAN_THE_TOTAL_AMOUNT_OF_LQT`                                        |                            |
 | 15   | `TOKEN_POOL_MINUS_TOKENS_WITHDRAWN_IS_NEGATIVE`                                        |                            |
 | 18   | `TOKENS_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_BOUGHT`                     |                            |
 | 19   | `TOKEN_POOL_MINUS_TOKENS_BOUGHT_IS_NEGATIVE`                                           |                            |
 | 20   | `ONLY_MANAGER_CAN_SET_BAKER`                                                           |                            |
 | 21   | `ONLY_MANAGER_CAN_SET_MANAGER`                                                         |                            |
 | 22   | `BAKER_PERMANENTLY_FROZEN`                                                             |                            |
 | 23   | `ONLY_MANAGER_CAN_SET_LQT_ADRESS`                                                      |                            |
 | 24   | `LQT_ADDRESS_ALREADY_SET`                                                              |                            |
 | 25   | `CALL_NOT_FROM_AN_IMPLICIT_ACCOUNT`                                                    |                            |
 | 28   | `INVALID_FA12_TOKEN_CONTRACT_MISSING_GETBALANCE`                                       | Only used by FA1.2 variant |
 | 28   | `INVALID_FA2_TOKEN_CONTRACT_MISSING_BALANCE_OF`                                        | Only used by FA2 variant   |
 | 29   | `THIS_ENTRYPOINT_MAY_ONLY_BE_CALLED_BY_GETBALANCE_OF_TOKENADDRESS`                     |                            |
 | 31   | `INVALID_INTERMEDIATE_CONTRACT`                                                        |                            |
 | 32   | `INVALID_FA2_BALANCE_RESPONSE`                                                         | Only used by FA2 variant   |
 | 33   | `UNEXPECTED_REENTRANCE_IN_UPDATE_TOKEN_POOL`                                           |                            |


In the informal specification of each entrypoint, we list the
conditions that will trigger an error if true (in the subsection
"Errors").

## FA1.2 interaction

When the CPMM contract is paired with an FA1.2 token contract, it will interact with
two of its entrypoints: `%getBalance` and `%transfer`.

We here describe how the CPMM contract interacts with FA1.2
entrypoints. For full details, see the [FA1.2 standard][fa12].

### `%getBalance`

```
(pair %getBalance (address :owner) (contract :callback nat))
```

`%getBalance` is a view entrypoint. The caller sends the address of
whose balance they want to inquire, the `:owner`, and a contract
entrypoint `:callback` of parameter `nat` where the balance
will be sent. It will return zero if the owner does not have a balance
in the FA1.2 token.

### `%transfer`

```
(pair %transfer (address :owner) (pair (address :to) (nat :value)))
```

`%transfer` transfers ownership of FA1.2 tokens from the `:owner`
address to the `:to` address under the following conditions: `sender`
is the `:owner` or has an `allowance` that is greater than or equal to
`:value` and `:value` is less than or equal to `:owner`'s balance.

## FA2 interaction

When the CPMM contract is paired with an FA2 token contract, it will
interact with two of its entrypoints: `%balance_of` and `%transfer`.
An FA2 token contract may hold multiple tokens, differentiated by
token ids.  The CPMM contract can only be paired with a single,
designated, FA2 `tokenAddress` and `tokenId` pair.

We here describe how the CPMM contract interacts with FA2
entrypoints. For full details, see the [FA2 standard][fa2].

### `%balance_of`

```
(pair %balance_of
   (list :requests (pair (address :owner) (nat :token_id)))
   (contract :callback
      (list (pair
               (pair :request (address :owner) (nat :token_id))
               (nat :balance)))))
```

`%balance_of` is a view entrypoint. This embodies the same idea as FA1.2's
`%getBalance`, but with the ability to run multiple balance queries for multiple
owners and token ids. CPMM will only emit FA2 `%balance_of` calls that
query a single balance of its designated token address and token id pair.

### `%transfer`

```
(list %transfer
  (pair (address :from_)
     (list :txs (pair (address :to_)
       (pair
         (nat :tokenId)
         (nat :amount))))))
```

FA2 `transfer` allows callers to perform multiple transfers. CPMM
will only emit FA2 `transfer` operations that perform a single
transfer of the designated token address and token id pair.

## Liquidity token contract interaction

The exchange interacts with the [Liquidity token contract][dexter2-lqt-fa12] exclusively
through the `%mintOrBurn` entrypoint:

- by "mint `amount` LQT and credit them to account `owner`" we mean
  calling this entrypoint with the parameter `Pair amount owner`;
- by "burn `amount` LQT and debit them from account `owner`" we mean
  calling this entrypoint with the parameter `Pair (-amount) owner`.

## Entrypoints

The CPMM contract has the following entrypoints.

* [`%addLiquidity`](#addliquidity-entrypoint)
* [`%removeLiquidity`](#removeliquidity-entrypoint)
* [`%xtzToToken`](#xtztotoken-entrypoint)
* [`%tokenToXtz`](#tokentoxtz-entrypoint)
* [`%setBaker`](#setbaker-entrypoint)
* [`%setManager`](#setmanager-entrypoint)
* [`%setLqtAddress`](#setlqtaddress-entrypoint)
* [`%default`](#default-entrypoint)
* [`%updateTokenPool`](#updatetokenpool-entrypoint)
* [`%updateTokenPoolInternal`](#updatetokenpoolinternal-entrypoint)
* [`%tokenToToken`](#tokentotoken-entrypoint)

## Storage

Note: The layout of the storage depends on [the variant of
the Dexter 2 instantiation](dexter2.md#variants) (whether compiled to pair FA1.2 or FA2
token contract). This is necessary to accommodate the differences
between FA1.2 and FA2 contracts.

An FA2 contract can hold multiple types of tokens, differentiated by a token id. An exchange
contract can only be paired with one of those tokens. The CPMM
contract paired with an FA2 needs to know the token
id in addition to the token address.

 | Name                      | Type      | Mutable | Comment                                                                                                                                                                                                                                                                                                                      |
 |:--------------------------|:----------|:--------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
 | `tokenPool`               | `nat`     | Yes     | Total amount of **TOK** held by the CPMM contract. The contract implicitly calculates the amount it holds without querying the token contract.  Note that true token balance of the CPMM may be higher, and that there are a few edge cases where it needs to query the token contract to update the value of **tokenPool**. |
 | `xtzPool`                 | `mutez`   | Yes     | Total amount of **XTZ** held by the CPMM contract.                                                                                                                                                                                                                                                                           |
 | `lqtTotal`                | `nat`     | Yes     | Total number of **LQT** tokens distributed by the CPMM contract to Liquidity providers.                                                                                                                                                                                                                                      |
 | `selfIsUpdatingPoolToken` | `bool`    | Yes     | Boolean flag indicating whether the CPMM should accept a call on the `%updateTokenPoolInternal` callback entrypoint from the token contract. Also, when true, only `%updateTokenPoolInternal` can be called, all other CPMM entrypoints fail.                                                                                |
 | `freezeBaker`             | `bool`    | Yes     | Boolean flag indicating whether the CPMM should be able to change the delegate.                                                                                                                                                                                                                                              |
 | `manager`                 | `address` | Yes     | Address of the manager.                                                                                                                                                                                                                                                                                                      |
 | `lqtAddress`              | `address` | Yes     | Address of the accompanying liquidity token contract                                                                                                                                                                                                                                                                         |
 | `tokenAddress`            | `address` | No      | Address of the FA1.2/FA2 token contract.                                                                                                                                                                                                                                                                                     |
 | `tokenId`                 | `nat`     | No      | **(Only in the FA2 version)** The token id of **TOK**, that is the designated FA2 token contract handled by the CPMM contract.                                                                                                                                                                                               |

### Storage invariants (true statements at the end of each contract execution)

* **lqtTotal** is strictly equal to the `totalSupply` field of the
  liquidity contract storage, and is always strictly positive.
* **xtzPool** is strictly equal to the **XTZ** balance of the CPMM
  contract, and is always strictly positive.
* **tokenPool** is lesser or equal to the amount of **TOK** held by
  the CPMM contract, according to the token storage, and is always
  strictly positive.

The CPMM contract enforces these invariants.  It is the responsability
of the exchange originator to initialize the exchange’s contracts such
that they are initially verified.

## Operations

The CPMM contract itself is identified in the token contract with its own
address at the entrypoint `%default`.  In other words, when transferring
tokens from (resp. to) the CPMM, the source (resp. target) address given as a
parameter in the call to the entrypoint transfer from the token contract is
obtained through `SELF; ADDRESS`.

## Fees

Fees are collected in the three types of trade at the following rates:

* `%xtzToToken`: 0.3% fee paid in **XTZ**.
* `%tokenToXtz`: 0.3% fee paid in tokens.
* `%tokenToToken`: 0.3% fee paid in **XTZ** (note that fees are applied
  again in the subsequent call to `%xtzToToken` on the target Dexter,
  see below for details)

Fees collected during trades are added to the **XTZ** and **TOK**
pools, but do not affect the number of **LQT** tokens (**lqtTotal**).

## Implicit parameters

For all entrypoint calls, we refer to the following implicit
parameters:

* **source**: the implicit contract that initiated the current chain
  of smart contract calls, as returned by the Michelson instruction
  [`SOURCE`][mref-SOURCE].
* **sender**: the contract that initiated the current internal
  transaction, as returned by the Michelson instruction
  [`SENDER`][mref-SENDER].
* **amount**: the amount of XTZ, in mutez, supplied in the transaction
  that initiated the current contract execution, as returned by the
  [`AMOUNT`][mref-AMOUNT] instruction.
* **now**: the current timestamp, as returned by the [`NOW`][mref-NOW]
  instruction.

## `%addLiquidity` entrypoint

Called to provide liquidity to the exchange. The caller (a liquidity
provider) provides an amount of **XTZ**. They will also be debited a
number of **TOK**, based on the exchange's current exchange rate
between **XTZ** and **TOK**. In return, the exchange mints a number of
**LQT** that are added to a designated account in the liquidity token
contract.

### Parameters

```michelson
(pair %addLiquidity
   (address %owner)
   (pair (nat %minLqtMinted) (pair (nat %maxTokensDeposited) (timestamp %deadline))))
```


 | Name                 | Type        |                                                          |
 |:---------------------|:------------|:---------------------------------------------------------|
 | `owner`              | `address`   | address of the account that will own the minted LQT.     |
 | `minLqtMinted`       | `nat`       | minimum number of LQT the caller requires to be minted.  |
 | `maxTokensDeposited` | `nat`       | maximum number of TOK deposited.                         |
 | `deadline`           | `timestamp` | timestamp after which this transaction will be rejected. |

Note: This will trigger an allowance check in the token contract for
transferring tokens from `owner` to CPMM, unless **sender** is
`owner`.

### Operations

The CPMM contract computes `tokens_deposited`. That is, the amount
of TOK that will be deposited from the `owner` to the CPMM contract.

1. The token contract transfer `tokens_deposited` from owner to
   exchange contract

### Logic

Note: Adding liquidity requires the sender contract to deposit an
equivalent exchange value of **XTZ** and **TOK**. Depositing `x`
**XTZ** requires to also deposit `y` **TOK**, where `x` **XTZ** would
be traded against `y` **TOK** by the exchange.

If the `deadline` has passed, i.e. `NOW >= deadline`, then the
transaction fails with error code
`THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`.

The sender provides a minimum amount of LQT they want to acquire
(`minLqtMinted`), an amount of **XTZ** (**amount**) and the maximum
number of **TOK** they want to deposit (`maxTokensDeposited`).

The amount of **LQT** minted is:

```
    lqt_minted = floor((amount * lqtTotal) / xtzPool)
```

If `lqt_minted` is less than the amount of **LQT** they want to
acquire, `minLqtMinted`, the transaction will fail, with error code
`LQT_MINTED_MUST_BE_GREATER_THAN_MIN_LQT_MINTED`. If `xtzPool` is
zero, the transaction fails with message `"DIV by 0"`. Note that
`xtzPool` should never be equal to zero, as it makes the CPMM
effectively inoperative.

The amount of token to be deposited is:

```
    tokens_deposited = ceil((tokenPool * amount) / xtzPool).
```

If `tokens_deposited` is greater than the maximum number they want to
provide, `maxTokensDeposited`, the transaction will fail with error
code
`MAX_TOKENS_DEPOSITED_MUST_BE_GREATER_THAN_OR_EQUAL_TO_TOKENS_DEPOSITED`.

An internal transaction is emitted to the token contract, instructing it to transfer
`tokens_deposited` tokens from **owner** to CPMM.

An internal transaction is emitted to the liquidity token contract, instructing it to
mint `lqt_minted` LQT and crediting them to the account of
**owner**.

Finally, the contract updates it's view of the pools with the added liquidity:

```
    lqtTotal := lqtTotal + lqt_minted
    tokenPool := tokenPool + tokens_deposited
    xtzPool := xtzPool + amount
```

### Errors

| Error condition                         | Error value                                                              |
|:----------------------------------------|:-------------------------------------------------------------------------|
| `selfIsUpdatingPoolToken == true`       | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE`                              |
| `NOW >= deadline`                       | `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`                        |
| `maxTokensDeposited < tokens_deposited` | `MAX_TOKENS_DEPOSITED_MUST_BE_GREATER_THAN_OR_EQUAL_TO_TOKENS_DEPOSITED` |
| `lqt_minted < minLqtMinted`             | `LQT_MINTED_MUST_BE_GREATER_THAN_MIN_LQT_MINTED`                         |
| `xtzPool == 0`                          | `"DIV by 0"`                                                             |

## `%removeLiquidity` entrypoint

**LQT** tokens can be burned in order to withdraw a proportional
amount of **XTZ** and token. They are withdrawn at the current
exchange rate.

### Parameters

``` michelson
(pair %removeLiquidity
  (address %to)
  (pair (nat %lqtBurned)
    (pair (mutez %minXtzWithdrawn) (pair (nat %minTokensWithdrawn) (timestamp %deadline)))))
```

 | Name                 | Type        |                                                                             |
 |:---------------------|:------------|:----------------------------------------------------------------------------|
 | `to`                 | `address`   | where to send the XTZ and tokens.                                           |
 | `lqtBurned`          | `nat`       | amount of LQT the sender wants to burn                                      |
 | `minXtzWithdrawn`    | `mutez`     | the minimum amount of XTZ (in Mutez) the sender wants to withdraw and send. |
 | `minTokensWithdrawn` | `nat`       | the minimum amount of tokens the sender wants to withdraw and send.         |
 | `deadline`           | `timestamp` | the time after which this transaction can no longer be executed.            |

### Logic

If `AMOUNT` is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

If the `deadline` has passed, i.e. `NOW >= deadline`, the transaction
fails with `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`.

The sender provides the amount of **LQT** they want to burn in the
`lqtBurned` parameter, and the minimum amount of **XTZ** and **TOK**
they want to withdraw, in the `minXtzWithdrawn` and
`minTokensWithdrawn` parameters respectively. If either limit is not
met the transaction fails.

The amount of **XTZ** withdrawn is

```
    xtz_withdrawn = floor((lqtBurned * xtzPool) / lqtTotal).
```


If `lqtTotal` is zero the transaction fails with error value `"DIV by
0"`.  Note that `lqtTotal` should never be equal to zero, as it makes
the CPMM effectively inoperative.

if `xtz_withdrawn < minXtzWithdrawn`, the transaction fails with
`THE_AMOUNT_OF_XTZ_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_WITHDRAWN`.

<!--
 !-- If `xtzPool < xtz_withdrawn`, the transaction fail.
  -->

If `lqtBurned > lqtTotal`, the contract fails with `CANNOT_BURN_MORE_THAN_THE_TOTAL_AMOUNT_OF_LQT`.

The amount of **TOK** withdrawn is:


```
    tokens_withdrawn = floor((lqtBurned * tokenPool) / lqtTotal).
```

If `tokens_withdrawn > minTokensWithdrawn`, the contract fails with
`THE_AMOUNT_OF_TOKENS_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_WITHDRAWN`.

If `tokens_withdrawn > tokenPool`, the contract fails with
`TOKEN_POOL_MINUS_TOKENS_WITHDRAWN_IS_NEGATIVE`.



<!--
 !-- An internal transaction is emitted to the token contract, transferring
 !-- `tokens_deposited` tokens from **owner** to CPMM.
 !--
 !-- An internal transaction is emitted to the liquidity token contract,
 !-- minting `lqt_minted` LQT and crediting them to the account of
 !-- **owner**.
  -->


If the type parameter of the contract associated to the address `to`
(as returned by the Michelson instruction [`CONTRACT`][mref-CONTRACT])
is not `unit`, then the transaction fails with `INVALID_TO_ADDRESS`.

An internal transaction is emitted to the liquidity token contract,
instructing it to burn `lqtBurned` **LQT** and debit them from the
account of **sender**.

An internal transaction is emitted to the token contract, instructing
it to transfer `tokens_withdrawn` **TOK** from the CPMM contract to
the `to` address.

An internal transaction is emitted, transferring `xtz_withdrawn`
**XTZ** from the CPMM contract to the `to` address.

Finally, the contract updates its view of the pools with the removed
liquidity:


```
    xtzPool := xtzPool - xtz_withdrawn
    lqtTotal := lqtTotal - lqtBurned
    tokenPool := tokenPool - tokens_withdrawn
```

### Operations

1. Burn `lqtBurned` from `to`'s account in the liquidity token contract
2. Token transfer `tokens_deposited` from CPMM to `to` address.
3. Send `xtz_withdrawn` from CPMM to `to` address.

### Errors

| Error condition                                       | Error value                                                                            |
|:------------------------------------------------------|:---------------------------------------------------------------------------------------|
| `selfIsUpdatingPoolToken == true`                               | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE`                                            |
| `NOW >= deadline`                                               | `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`                                      |
| `AMOUNT > 0`                                                    | `AMOUNT_MUST_BE_ZERO`                                                                  |
| `xtz_withdrawn < minXtzWithdrawn`                               | `THE_AMOUNT_OF_XTZ_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_WITHDRAWN`       |
| `lqtTotal == 0` (implicit from the division)                    | `"DIV by 0"`                                                                           |
| `tokens_withdrawn < minTokensWithdrawn`                         | `THE_AMOUNT_OF_TOKENS_WITHDRAWN_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_WITHDRAWN` |
| `tokens_withdrawn > tokenPool`                                  | `TOKEN_POOL_MINUS_TOKENS_WITHDRAWN_IS_NEGATIVE`                                        |
| `xtz_withdrawn > xtzPool`                                       | Run-time error in mutez subtraction                                                    |
| `lqtBurned > lqtTotal`                                          | `CANNOT_BURN_MORE_THAN_THE_TOTAL_AMOUNT_OF_LQT`                                        |
| `to` address’ contract does not have the parameter type `unit`. | `INVALID_TO_ADDRESS`                                                                   |

## `%xtzToToken` entrypoint

### Parameters

``` michelson
(pair %xtzToToken (address %to) (pair (nat %minTokensBought) (timestamp %deadline)))
```

 | Name              | Type        |                                                                  |
 |:------------------|:------------|:-----------------------------------------------------------------|
 | `to`              | `address`   | address to send the tokens to.                                   |
 | `minTokensBought` | `nat`       | minimum amount of tokens the sender wants to buy.                |
 | `deadline`        | `timestamp` | the time after which this transaction can no longer be executed. |

### Logic

If the deadline has passed, i.e. `NOW >= deadline`, the
transaction fails with
`THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`.

A fee of %0.3 is taken. This is implemented as follows:


```
    tokens_bought = floor((amount * 997 * tokenPool) /
                          (xtzPool * 1000 + (amount * 997)))
```

`tokens_bought` must be greater than or equal to **minTokensBought**
or the transaction fails with
`TOKENS_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_BOUGHT`.

`tokenPool` must be larger or equal to `tokens_bought`, or the
transaction fails with `TOKEN_POOL_MINUS_TOKENS_BOUGHT_IS_NEGATIVE`.

The `to` address receives `tokens_bought` **TOK**. This is implemented
by the emission of an internal operation that instructs the token
contract to transfer `tokens_bought` tokens from the CPMM contract to
the `to` address.

Finally, the contract updates its view of the pools:

    xtzPool := xtzPool + amount
    tokenPool := tokenPool - tokens_bought

### Operations

1. Token transfer `tokens_bought` from CPMM contract to the `to` address.

### Errors

| Error condition                   | Error value                                                        |
|:----------------------------------|:-------------------------------------------------------------------|
| `selfIsUpdatingPoolToken == true` | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE`                        |
| `NOW >= deadline`                 | `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`                  |
| `xtzPool == 0 && amount == 0`     | Run-time error in division                                         |
| `minTokensBought > tokens_bought` | `TOKENS_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_TOKENS_BOUGHT` |
| `tokens_bought > tokenPool`       | `TOKEN_POOL_MINUS_TOKENS_BOUGHT_IS_NEGATIVE`                       |

## `%tokenToXtz` entrypoint

### Parameters

``` michelson
(pair %tokenToXtz
  (address %to)
  (pair (nat %tokensSold) (pair (mutez %minXtzBought) (timestamp %deadline))))
```

 | Name           | Type        |                                                                  |
 |:---------------|:------------|:-----------------------------------------------------------------|
 | `to`           | `address`   | address to send the XTZ tokens to.                               |
 | `tokensSold`   | `nat`       | the number of TOK the sender wants to sell.                      |
 | `minXtzBought` | `mutez`     | minimum amount of XTZ (in Mutez) the sender wants to purchase.   |
 | `deadline`     | `timestamp` | the time after which this transaction can no longer be executed. |

Note: Provoke allowance check in the tokens contract for transferring
tokens from the owner to the exchange, which means traders are
expected to provide an allowance of `tokensSold` to the CPMM contract
thanks to the token contract.

### Logic

If the deadline has passed, i.e. `NOW >= deadline`, the
transaction fails with
`THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`.

If `AMOUNT` is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

A fee of %0.3 is taken. This is implemented as follows:

```
    xtz_bought = floor((tokensSold * 997 * xtzPool) /
            (tokenPool * 1000 + (tokensSold * 997)))
```

If `xtz_bought < minXtzBought`, the contract fails with
`XTZ_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_BOUGHT`.

If the type parameter of the contract associated to the address `to`
(as returned by the Michelson instruction [`CONTRACT`][mref-CONTRACT])
is not `unit`, then the transaction fails with `INVALID_TO_ADDRESS`.

The `to` address receives **XTZ** equivalent to `xtz_bought`. This is
implemented by the emission of an internal operation that transfers
this sum to the `to` address.

The CPMM contract receives `tokensSold` **TOK**. This is implemented
by the emission of an internal operation that calls the token
contract, instructing it to transfer `tokensSold` **TOK** from the
`sender` to the CPMM contract.

Finally, the contract updates its view of the pools:

    tokenPool := tokenPool + tokensSold
    xtzPool := xtzPool - xtz_bought

### Operations

1. Token transfer `tokensSold` **TOK** from `to` address to the CPMM
   contract.
2. Transfer `xtz_bought` **XTZ** from the CPMM contract to `to`
   address.

### Errors

| Error condition                      | Error value                                                  |
|:-------------------------------------|:-------------------------------------------------------------|
| `selfIsUpdatingPoolToken == true`    | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE`                  |
| `NOW >= deadline`                    | `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`            |
| `minXtzBought > xtz_bought`          | `XTZ_BOUGHT_MUST_BE_GREATER_THAN_OR_EQUAL_TO_MIN_XTZ_BOUGHT` |
| `AMOUNT > 0`                         | `AMOUNT_MUST_BE_ZERO`                                        |
| `tokenPool == 0 && tokensSold == 0` | Run-time error in division                                   |

## `%tokenToToken` entrypoint

`%tokenToToken` allows two exchanges to interoperate and perform a
trade between them.  In this context, it means an owner of **TOK** can
trade it for another token (referred to as **TOK2**) in a single
transaction (intuitively, the same as calling `%tokenToXtz` in the
first exchange followed by `%xtzToToken` in the second).

### Parameters

``` michelson
(pair %tokenToToken
  (address %outputDexterContract)
  (pair (nat %minTokensBought)
    (pair (address %to) (pair (nat %tokensSold) (timestamp %deadline)))))
```

 | Name                   | Type        |                                                                                         |
 |:-----------------------|:------------|:----------------------------------------------------------------------------------------|
 | `outputDexterContract` | `address`   | address of CPMM of the intermediate exchange of token that the sender wants to purchase |
 | `minTokensBought`      | `nat`       | minimum amount of TOK2 tokens the sender will purchase from outputDexterContract        |
 | `to`                   | `address`   | the address that owns the TOK tokens that are being sold                                |
 | `tokensSold`           | `nat`       | the number of TOK tokens the sender wants to sell                                       |
 | `deadline`             | `timestamp` | the time after which this transaction can no longer be executed.                        |

Note: Requires allowance check in the token contract for transferring
**TOK** from the **sender** to the CPMM contract, and for the
`outputDexterContract` to the `to` address.

### Logic

If `outputDexterContract` is not a contract with an `%xtzToToken`
entrypoint of the same type as the CPMM's entrypoint of the same name,
then fail with `INVALID_INTERMEDIATE_CONTRACT`.

If **amount** is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

If the deadline has passed, i.e. `NOW >= deadline`, the
transaction fails with
`THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE`.

<!--
 !-- If `minTokensBought` is zero, the transaction will fail.
  -->

If both `tokenPool` and `tokensSold` is zero, the transaction will fail with
a division run-time error.

If `xtzPool` is zero, the transaction will fail with
a mutez subtraction run-time error.


The intermediate sum of `xtz_bought` is calculated thus:

```
xtz_bought = floor((tokensSold * 997 * xtzPool) /
                   (tokenPool * 1000 + (tokensSold * 997)))
```

The CPMM contract receives `tokensSold` **TOK**. This is implemented
by the emission of an internal operation that calls token contract,
instructing it to transfer `tokensSold` **TOK** from the **sender** to
the CPMM contract.

Then, a call to the `%xtzToToken` entrypoint of the `outputDexterContract`
is emitted with the following parameters: `to`, `minTokensBought` and
`deadline`, and attaching the XTZ amount `xtz_bought`.

Finally, the contract updates its view of the pools:

```
tokenPool :=  tokenPool + tokensSold
xtzPool := xtzPool - xtz_bought
```

### Operations

1. Token transfer `tokensSold` **TOK** from `to` address to the CPMM
   contract.
2. A call to `output_dexter_address%xtzToToken` with parameters `to`,
   `minTokensBought` and `deadline` attaching the **XTZ** amount
   `xtz_bought`.

### Errors

| Error condition                                             | Error value                                       |
|:------------------------------------------------------------|:--------------------------------------------------|
| `selfIsUpdatingPoolToken == true`                           | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE`       |
| `NOW >= deadline`                                           | `THE_CURRENT_TIME_MUST_BE_LESS_THAN_THE_DEADLINE` |
| `AMOUNT > 0`                                                | `AMOUNT_MUST_BE_ZERO`                             |
| `xtzBought > xtzPool`                                       | Run-time error in mutez subtraction               |
| `tokenPool == 0 && tokensSold == 0`                         | Run-time error in division                        |
| `to` does not have `%xtzToToken` entrypoint of correct type | `INVALID_INTERMEDIATE_CONTRACT`                   |

## `%setBaker` entrypoint

The manager has the right to set the baker (delegate) for the CPMM
contract of the exchange. The baker can be frozen by setting the
parameter `freezeBaker = true`, after which subsequent calls to this
entrypoint will fail.

The motivation behind freezing the baker is to give up the manager rights
and set a virtual baker when it is supported in the protocol
(discussion about
[virtual bakers](https://forum.tezosagora.org/t/on-the-tezos-delegation-model/1562/4)).

### Parameters

``` michelson
(pair %setBaker (option %baker key_hash) (bool %freezeBaker))
```

 | Name          | Type              |                             |
 |:--------------|:------------------|:----------------------------|
 | `new_baker`   | `option key_hash` | The new baker to set        |
 | `freezeBaker` | `bool`            | Whether to freeze the baker |

### Logic

If **amount** is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

If `sender == manager` and not `freezeBaker` then

```
    if (freezeBaker) then freezeBaker := true
```

and emit a `SET_DELEGATE` operation setting the baker to `new_baker`.

Otherwise fail.

### Operations

1. `SET_DELEGATE` with `new_baker`

### Errors

| Error condition                   | Error value                                 |
|:----------------------------------|:--------------------------------------------|
| `selfIsUpdatingPoolToken == true` | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE` |
| `AMOUNT > 0`                      | `AMOUNT_MUST_BE_ZERO`                       |
| `sender != manager`               | `ONLY_MANAGER_CAN_SET_BAKER`                |
| The baker is frozen               | `BAKER_PERMANENTLY_FROZEN`                  |

## `%setManager` entrypoint

The manager can set a new address as the manager of the exchange.

### Parameters

``` michelson
(address %setManager)
```

 | Name          | Type      |                        |
 |:--------------|:----------|:-----------------------|
 | `new_manager` | `address` | The new manager to set |

### Logic

If **amount** is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

If `sender == manager`

```
    manager := new_manager
```

Otherwise fail with `ONLY_MANAGER_CAN_SET_MANAGER`.

### Operations

None.

### Errors

| Error condition                   | Error value                                 |
|:----------------------------------|:--------------------------------------------|
| `selfIsUpdatingPoolToken == true` | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE` |
| `AMOUNT > 0`                      | `AMOUNT_MUST_BE_ZERO`                       |
| `sender != current manager`       | `ONLY_MANAGER_CAN_SET_MANAGER`              |

## `%setLqtAddress` entrypoint

The manager can change the address of the associated liquidity token contract.

### Parameters

``` michelson
(address %setLqtAddress)
```

 | Name             | Type      |                                     |
 |:-----------------|:----------|:------------------------------------|
 | `new_lqtAddress` | `address` | The liquidity token contract to set |

### Logic

If `AMOUNT` is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

If `sender != manager`, fail with  `ONLY_MANAGER_CAN_SET_LQT_ADRESS`.

If the current value of `lqtAddress` in storage is not the null
address (`tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU`), fail with
`LQT_ADDRESS_ALREADY_SET`.

Otherwise, update the storage:

```
    lqtAddress := new_lqtAddress
```

### Operations

None.

### Errors

| Error condition                                      | Error value                                 |
|:-----------------------------------------------------|:--------------------------------------------|
| `selfIsUpdatingPoolToken == true`                    | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE` |
| `AMOUNT > 0`                                         | `AMOUNT_MUST_BE_ZERO`                       |
| `sender != current manager`                          | `ONLY_MANAGER_CAN_SET_LQT_ADRESS`           |
| `lqtAddress != tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU` | `LQT_ADDRESS_ALREADY_SET`                   |

## `%default` entrypoint

An empty entrypoint that takes a value of `unit` as a parameter. It
allows the CPMM to receive **XTZ**.

### Parameter

``` michelson
(unit %default)
```

 | Name | Type |   |
 |:-----|:-----|:--|
 |      | unit |   |

### Logic


```
	xtzPool += amount
```

### Operations

None.

### Errors

| Error condition                   | Error value                                 |
|:----------------------------------|:--------------------------------------------|
| `selfIsUpdatingPoolToken == true` | `SELF_IS_UPDATING_TOKEN_POOL_MUST_BE_FALSE` |

## `%updateTokenPool` entrypoint

There are a few edge cases where **tokenPool** can become out of sync
with the value that the exchange actually holds in **TOK**.

For example, someone sends the CPMM contract TOK for free by
interacting directly with the token contract, or the token contract
has some behavior where the owner’s token automatically increases or
decreases.

This entrypoint updates `tokenPool` in the CPMM contract to reflect
its token balance in the token contract.

### Parameters

``` michelson
(unit %updateTokenPool)
```

 | Name | Type   |   |
 |:-----|:-------|:--|
 |      | `unit` |   |

### Logic

Only implicit accounts are allowed to call `%updateTokenPool`. Smart
contracts, on the other hand, are prevented from using this
entrypoint. This is enforced by checking that `sender == source`
(where **sender** resp. **source** are the values returned by the
Michelson instructions [`SENDER`][mref-SENDER] resp.
[`SOURCE`][mref-SOURCE] ), since only implicit accounts can be the
**source** of a transaction.

The rationale behind this limitation is to prevent smart contracts from
issuing adversarial operations that would be executed between
legitimate operations issued by the CPMM and the token contracts, as
these adversarial operations could desynchronize the CPMM and the
token contracts.


```
    assert (sender == source)
```

If **amount** is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

If `selfIsUpdatingPoolToken` is true, fail with `UNEXPECTED_REENTRANCE_IN_UPDATE_TOKEN_POOL`.

To prepare for `%updateTokenBalanceInternal`, set the storage value of `selfIsUpdatingPoolToken` to true:

    selfIsUpdatingPoolToken := true

If paired with an FA1.2 contract, emit an internal operation calling
the `%getBalance` entrypoint of the token contract and pass in the
address of the Dexter contract at `%updateTokenBalanceInternal`
entrypoint:

```
    transfer(((), self%updateTokenPoolInternal), 0, tokenAddress%getBalance)
```

If the `tokenAddress` does not have a `%getBalance` entrypoint of the
type specified by the [FA1.2][fa12] interface, fail with
`INVALID_FA12_TOKEN_CONTRACT_MISSING_GETBALANCE`.

Otherwise, if paired with an FA2 contract, emit an internal operation
calling the `%balance_of` entrypoint of the token contract and pass in
the address of the Dexter contract at Update Token Balance Internal
entrypoint:

```
    transfer(([(self, tokenId)], self%updateTokenPoolInternal), 0, tokenAddress%balance_of)
```

If the `tokenAddress` does not have a `%balance_of` entrypoint of the
type specified by the [FA2][fa2] interface, fail with
`INVALID_FA2_TOKEN_CONTRACT_MISSING_BALANCE_OF`.


### Operations

If paired with FA1.2:

1. Call token %getBalance for CPMM's address. Set the corresponding
   callback to the entrypoint `%updateTokenPoolInternal` of the CPMM
   contract itself, attaching 0 mutez.

If paired with FA2:

1. Call token %balance_of for CPMM's address and the stored
   `tokenId`. Set the corresponding callback to the entrypoint
   `%updateTokenPoolInternal` of the CPMM contract itself, attaching
   0 mutez.

### Errors

| Error condition                         | Error value                                 |
|:----------------------------------------|:--------------------------------------------|
| `source != sender`                      | `CALL_NOT_FROM_AN_IMPLICIT_ACCOUNT`         |
| `AMOUNT > 0`                            | `AMOUNT_MUST_BE_ZERO`                       |
| `selfIsUpdatingPoolToken == true`       | `UNEXPECTED_REENTRANCE_IN_UPDATE_TOKEN_POOL` |

## `%updateTokenPoolInternal` entrypoint

This is the entrypoint used as a callback to the call emitted to
the token contract in `%updateTokenPool`. Only the token contract can
call this entrypoint. Calls are only accepted when
`selfIsUpdatingPoolToken` is true.

### Parameters

The parameter is different depending on which token contract the
exchange interfaces.

#### For FA1.2

``` michelson
(nat %updateTokenPoolInternal)
```

 | Name       | Type  |                                      |
 |:-----------|:------|:-------------------------------------|
 | `response` | `nat` | Amount of token **TOK** held by the exchange |

#### For FA2

``` michelson
(list %updateTokenPoolInternal (pair (pair address nat) nat))
```

 | Name       | Type                             |                                                                                       |
 |:-----------|:---------------------------------|:--------------------------------------------------------------------------------------|
 | `response` | `list ((address, tokenId), nat)` | Amount of token **TOK** (identified by the `tokenId` identifier) held by the exchange |

In FA2, the parameter `response` should be a non-empty list.

### Logic

First, assert that the **sender** is `tokenAddress` and that
`selfIsUpdatingPoolToken` is true. If not, fail with
`THIS_ENTRYPOINT_MAY_ONLY_BE_CALLED_BY_GETBALANCE_OF_TOKENADDRESS`.

If **AMOUNT** is greater than zero, fail with `AMOUNT_MUST_BE_ZERO`.

Then, update the `tokenPool` as per the parameter, and reset the
`selfIsUpdatingPoolToken` flag:

#### FA1.2

```
    tokenPool := response
    self_is_updating_token_pool := false
```

#### FA2

Get first item of the response, `head`. If it does not exist, fail.
Otherwise, set `tokenPool` to the second component of `head`:

```
    tokenPool := snd head
    self_is_updating_token_pool := false
```

### Operations

None.

### Errors

| Error condition                                                 | Error value                                                        |
|:----------------------------------------------------------------|:-------------------------------------------------------------------|
| `self_is_updating_token_pool != true or sender != tokenAddress` | `THIS_ENTRYPOINT_MAY_ONLY_BE_CALLED_BY_GETBALANCE_OF_TOKENADDRESS` |
| `AMOUNT > 0`                                                    | `AMOUNT_MUST_BE_ZERO`                                              |

#### FA2

| Error condition              | Error value                    |
|:-----------------------------|:-------------------------------|
| the list `response` is empty | `INVALID_FA2_BALANCE_RESPONSE` |

[fa12]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md "TZIP-7: The FA1.2 standard"
[fa2]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md "TZIP-12: The FA2 standard"

[mref-AMOUNT]: https://tezos.gitlab.io/michelson-reference/#instr-AMOUNT "The Michelson AMOUNT instruction"
[mref-BALANCE]: https://tezos.gitlab.io/michelson-reference/#instr-BALANCE "The Michelson BALANCE instruction"
[mref-CONTRACT]: https://tezos.gitlab.io/michelson-reference/#instr-CONTRACT "The Michelson CONTRACT instruction"
[mref-FAILWITH]: https://tezos.gitlab.io/michelson-reference/#instr-FAILWITH "The Michelson FAILWITH instruction"
[mref-SENDER]: https://tezos.gitlab.io/michelson-reference/#instr-SENDER "The Michelson SENDER instruction"
[mref-SOURCE]: https://tezos.gitlab.io/michelson-reference/#instr-SOURCE "The Michelson SOURCE instruction"
[mref-NOW]: https://tezos.gitlab.io/michelson-reference/#instr-NOW "The Michelson NOW instruction"

[dexter2]: dexter2.md
[dexter2-cpmm]: dexter2-cpmm.md
[dexter2-terminology]: dexter2-terminology.md
[dexter2-lqt-fa12]: dexter2-lqt-fa12.md
