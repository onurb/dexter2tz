# How to integrate a Dexter exchange into your application

This page documents only the parts of Dexter that are needed to integrate it in an application. A more detailed specification of Dexter covering all aspects of the
contracts is available in the [informal specification](./informal-spec/dexter2.md).

Examples are provided using the the Tezos Client. How to call a smart contract from a dApp is outside of the scope of this document.

All information required to integrate a Dexter exchange can be obtained from the address of the exchange contract. This includes the token contract and liquidity token contract addresses. To integrate a Dexter exchange in an application, it is necessary to:
- Query the state of the various contracts and big maps
- Interact with the contracts by calling some of their entrypoints

## FA1.2/LQT storage

```
(pair (big_map %tokens address nat)
          (pair (big_map %allowances (pair (address %owner) (address %spender)) nat)
                (pair (address %admin) (nat %total_supply))))
```

- `tokens` a mapping of how much an address owns and allowances that allow other contracts to spend the owners token
- `allowances` a mapping of `owner` and `spender` addresses and amounts approved for token transfers.
- `admin` the account able to mint and burn tokens.
- `total_supply` the total amount of token that exists. This is also equal to the sum of all account balances.

## Dexter storage

```
(pair (nat %tokenPool)
          (pair (mutez %xtzPool)
                (pair (nat %lqtTotal)
                      (pair (bool %selfIsUpdatingTokenPool)
                            (pair (bool %freezeBaker)
                                  (pair (address %manager) (pair (address %tokenAddress) (address %lqtAddress))))))))
```

- `tokenPool` total amount of tokens available to trade.
- `xtzPool` total amount of tez available to trade.
- `lqtTotal` total liquidity tokens outstanding.
- `selfIsUpdatingTokenPool` used internally by Dexter when updating tokenPool.
- `freezeBaker` used internally by Dexter when setting a delegate.
- `manager` the account able to update the delegate.
- `tokenAddress` address of the token contract traded against tez.
- `lqtAddress` address of the liquidity token contract

## Dexter Entry Points


### addLiquidity

The user will provide you with an XTZ amount and/or an FA1.2 amount.
Given an XTZ amount they want to deposit you can calculate how much
FA1.2 they need to provide and vice versa. You can also calculate how
much liquidity (LQT) will be minted.

- the `lqt_total`, `token_pool`, and `xtz_pool` from dexter storage.
- the `account` address that is depositing liquidity in dexter.
- the amount of tez the user wants to deposit, `xtz_deposit`

Given the amount of tez the user wants to deposit, we can calculate the amount 
of FA1.2 that would be required (tokens_deposited).

Note: unless otherwise mentioned, in all of the examples below division is rounded down.

```
xtz_pool = user input of xtz

// division rounded up
tokens_deposited = (xtz_deposited * token_pool) / xtz_pool

lqt_minted = xtz_deposited * lqt_total / xtz_pool
```

You will also need to pick a deadline for when the transaction sure occur latest 
(this is the case for most of the Dexter entry points).

```
deadline = now + amount_of_time
```

#### tezos client example

```bash
$ tezos-client call <token-contract-name> \ 
               from <sender> \
               --entrypoint 'approve' \
               --arg 'Pair "<exchange-contract-address>" <max_tokens_deposited>' \
               --burn-cap 1
			   
$ tezos-client transfer <xtz> 
               from <sender> \ 
               to <exchange-contract-name> \
               --entrypoint 'addLiquidity' \
               --arg 'Pair "<owner>" <min_lqt_created> <max_tokens_deposited> "<deadline>"' \
               --burn-cap 1
```

### removeLiquidity

Give an amount of liquidity that the user wants to remove we can calculate
the amount of XTZ and FA1.2 that will be returned. You need to the following
data:

- `lqt_burned`: the number of LQT tokens the user wants to remove.
- `xtz_pool`: the `xtz` balance from dexter
- `token_pool`: from dexter storage.
- `lqt_total`: from dexter storage.

```
xtz_withdrawn    = lqt_burned * xtz_pool   / lqt_total

tokens_withdrawn = lqt_burned * token_pool / lqt_total
```

#### tezos client example

```bash
$ tezos-client call <exchange-contract-name> \
               from <sender> \
               --entrypoint 'removeLiquidity' \
               --arg 'Pair "<to>" <lqt_burned> <min_xtz_withdrawn> <min_tokens_withdrawn> "<deadline>"' \
               --burn-cap 1
```

### xtzToToken

Given an amount of XTZ the user wants to sell, we can caculate how much token
they will receive, and vice-versa. There is a 0.3% fee taken from the XTZ amount
sold and the amount sold is added to the xtz_pool. 

You need to know the following:

- `xtz_sold`: amount of XTZ the user is selling
- `token_pool`: from the dexter storage
- `xtz_pool`: balance from dexter contract

```
tokens_bought = (xtz_sold * token_pool * 0.997) / (xtz_pool + xtz_sold * 0.997)
```

If you want to calculate how much XTZ sold to get a certain amount of tokens:

- `tokens_bought`: amount of tokens the user wants to buy

```
xtz_sold = (xtz_pool * tokens_bought * 0.997) / (token_pool + tokens_bought * 0.997)
```

#### tezos client example

```bash
$ tezos-client transfer <xtz> \
               from <buyer> \ 
               to <exchange-contract-name> \
               --entrypoint 'xtzToToken' \
               --arg 'Pair "<to>" <min-tokens-required> "<deadline>"' \
               --burn-cap 1
```

### tokenToXtz

You need to give Dexter permission to sell any particular amount of FA1.2 token.

Given an amount of FA1.2 the user wants to sell, we can caculate how much XTZ
they will receive, and vice-versa. There is a 0.3% fee taken from the FA1.2 amount
sold and the amount sold is added to token_pool. 

You need to know the following:

- `token_sold`: amount of FA1.2 the user is selling
- `token_pool`: from the dexter storage
- `xtz_pool`: balance from dexter contract

```
xtz_bought = (tokens_sold * 0.997 * xtz_pool) / token_pool + (tokens_sold * 0.997)
```

If you want to calculate how much FA1.2 sold to get a certain amount of tokens:

- `xtz_bought`: amount of XTZ the user wants to buy.

```
tokens_sold = (xtz_bought * token_pool * 0.997) / (xtz_pool + xtz_bought * 0.997)
```

#### tezos client example

```bash
$ tezos-client call <token-contract-name> \ 
               from <sender> \
               --entrypoint 'approve' \
               --arg 'Pair "<exchange-contract-address>" <max_tokens_deposited>' \
               --burn-cap 1

$ tezos-client call <exchange-contract-name> \
               from <sender> \
               --entrypoint 'tokenToXtz' \
               --arg 'Pair "<to>" <tokens_sold> <min_xtz_bought> "<deadline>"' \
              --burn-cap 1
```
